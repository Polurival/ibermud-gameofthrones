package com.anapioficeandfire.gameofthrones.network;

import com.anapioficeandfire.gameofthrones.utils.ApplicationConfiguration;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static OkHttpClient.Builder sHttpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder sBuilder = new Retrofit.Builder()
            .baseUrl(ApplicationConfiguration.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        //sHttpClient.addInterceptor(new HeaderInterceptor());
        sHttpClient.addInterceptor(loggingInterceptor);
        sHttpClient.connectTimeout(ApplicationConfiguration.MAX_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        sHttpClient.readTimeout(ApplicationConfiguration.MAX_READ_TIMEOUT, TimeUnit.MILLISECONDS);
        //sHttpClient.cache(new Cache(GotApplication.getContext().getCacheDir(),
        //        Integer.MAX_VALUE));
        //sHttpClient.addNetworkInterceptor(new StethoInterceptor());

        Retrofit retrofit = sBuilder
                .client(sHttpClient.build())
                .build();

        return retrofit.create(serviceClass);
    }
}
