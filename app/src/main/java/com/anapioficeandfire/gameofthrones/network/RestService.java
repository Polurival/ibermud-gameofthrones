package com.anapioficeandfire.gameofthrones.network;

import com.anapioficeandfire.gameofthrones.network.requests.CharacterModelRes;
import com.anapioficeandfire.gameofthrones.network.requests.HouseModelRes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RestService {

    @GET
    Call<HouseModelRes> getHouse();

    @GET
    Call<CharacterModelRes> getCharacter();

    @GET("houses/{id}")
    Call<HouseModelRes> getHouseById(@Path("id") int id);

    @GET("characters/{id}")
    Call<CharacterModelRes> getCharacterById(@Path("id") int id);
}
