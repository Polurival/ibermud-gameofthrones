
package com.anapioficeandfire.gameofthrones.network.requests;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class CharacterModelRes {

    @SerializedName("url")
    @Expose
    public String url;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("culture")
    @Expose
    public String culture;

    @SerializedName("born")
    @Expose
    public String born;

    @SerializedName("died")
    @Expose
    public String died;

    @SerializedName("titles")
    @Expose
    public List<String> titles = new ArrayList<String>();

    @SerializedName("aliases")
    @Expose
    public List<Object> aliases = new ArrayList<Object>();

    @SerializedName("father")
    @Expose
    public String father;

    @SerializedName("mother")
    @Expose
    public String mother;

    @SerializedName("spouse")
    @Expose
    public String spouse;

    @SerializedName("allegiances")
    @Expose
    public List<String> allegiances = new ArrayList<String>();

    @SerializedName("books")
    @Expose
    public List<String> books = new ArrayList<String>();

    @SerializedName("povBooks")
    @Expose
    public List<Object> povBooks = new ArrayList<Object>();

    @SerializedName("tvSeries")
    @Expose
    public List<Object> tvSeries = new ArrayList<Object>();

    @SerializedName("playedBy")
    @Expose
    public List<Object> playedBy = new ArrayList<Object>();

    public CharacterModelRes(String name, List<Object> aliases) {
        this.name = name;
        this.aliases = aliases;
    }

    public List<Object> getAliases() {
        return aliases;
    }

    public List<String> getAllegiances() {
        return allegiances;
    }

    public List<String> getBooks() {
        return books;
    }

    public String getBorn() {
        return born;
    }

    public String getCulture() {
        return culture;
    }

    public String getDied() {
        return died;
    }

    public String getFather() {
        return father;
    }

    public String getGender() {
        return gender;
    }

    public String getMother() {
        return mother;
    }

    public String getName() {
        return name;
    }

    public List<Object> getPlayedBy() {
        return playedBy;
    }

    public List<Object> getPovBooks() {
        return povBooks;
    }

    public String getSpouse() {
        return spouse;
    }

    public List<String> getTitles() {
        return titles;
    }

    public List<Object> getTvSeries() {
        return tvSeries;
    }

    public String getUrl() {
        return url;
    }
}