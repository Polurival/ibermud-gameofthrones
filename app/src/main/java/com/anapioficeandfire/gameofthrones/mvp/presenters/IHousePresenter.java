package com.anapioficeandfire.gameofthrones.mvp.presenters;

import android.support.annotation.Nullable;

import com.anapioficeandfire.gameofthrones.mvp.models.IHouseModel;
import com.anapioficeandfire.gameofthrones.mvp.views.IHouseView;

public interface IHousePresenter {
    void takeView(IHouseView houseView);
    void dropView();
    void initView();

    @Nullable
    IHouseView getView();
    IHouseModel getModel();

    void clickOnHouse();
    void clickOnMember();
}
