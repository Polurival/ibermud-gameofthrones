package com.anapioficeandfire.gameofthrones.mvp.models;

import com.anapioficeandfire.gameofthrones.data.managers.DataManager;
import com.anapioficeandfire.gameofthrones.data.storage.models.Character;
import com.anapioficeandfire.gameofthrones.data.storage.models.House;

import java.util.List;

public class SplashModel implements ISplashModel {

    private static SplashModel sGotLab;
    private DataManager mDataManager;

    private SplashModel() {
        mDataManager = DataManager.getInstance();
    }

    public static SplashModel getInstance() {
        if (sGotLab == null) {
            sGotLab = new SplashModel();
        }
        return sGotLab;
    }

    @Override
    public void loadData() {
        mDataManager.loadData();
    }

    @Override
    public House getHouse(int id) {
        return mDataManager.getHouse(id);
    }

    @Override
    public Character getCharacter(int id) {
        return mDataManager.getCharacter(id);
    }


    @Override
    public List<House> getHouses() {
        return mDataManager.getHouses();
    }

    @Override
    public List<Character> getHouseMembers(House house) {
        return mDataManager.getHouseMembers(house);
    }

}
