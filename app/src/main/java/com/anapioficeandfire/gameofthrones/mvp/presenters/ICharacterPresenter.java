package com.anapioficeandfire.gameofthrones.mvp.presenters;

import android.support.annotation.Nullable;

import com.anapioficeandfire.gameofthrones.mvp.models.ICharacterModel;
import com.anapioficeandfire.gameofthrones.mvp.views.ICharacterView;

public interface ICharacterPresenter {
    void takeView(ICharacterView characterView);
    void dropView();
    void initView();

    @Nullable
    ICharacterView getView();
    ICharacterModel getModel();

    void clickOnParent();
    void clickOnMather();
}
