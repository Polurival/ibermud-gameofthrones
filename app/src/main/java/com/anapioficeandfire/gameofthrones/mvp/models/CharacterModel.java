package com.anapioficeandfire.gameofthrones.mvp.models;

import android.support.annotation.Nullable;

import com.anapioficeandfire.gameofthrones.data.managers.DataManager;
import com.anapioficeandfire.gameofthrones.data.storage.models.Character;
import com.anapioficeandfire.gameofthrones.utils.ConstantsManager;

public class CharacterModel implements ICharacterModel {

    private static CharacterModel sCharacterModel;
    private DataManager mDataManager;
    private Character mCharacter;

    private CharacterModel() {
        mDataManager = DataManager.getInstance();
    }

    public static CharacterModel getInstance() {
        if (sCharacterModel == null) {
            sCharacterModel = new CharacterModel();
        }
        return sCharacterModel;
    }

    @Override
    public Character getCharacter(int characterId) {
        mCharacter = mDataManager.getCharacter(characterId);
        return mCharacter;
    }

    @Nullable
    @Override
    public Character getCharacterFather() {
        Character father = null;

        if (mCharacter != null && mCharacter.getFatherId() != ConstantsManager.NULL_INDEX) {
            father = mDataManager.getCharacter(mCharacter.getFatherId());
        }

        return father;
    }

    @Nullable
    @Override
    public Character getCharacterMather() {
        Character mather = null;

        if (mCharacter != null && mCharacter.getMotherId() != ConstantsManager.NULL_INDEX) {
            mather = mDataManager.getCharacter(mCharacter.getMotherId());
        }

        return mather;
    }
}
