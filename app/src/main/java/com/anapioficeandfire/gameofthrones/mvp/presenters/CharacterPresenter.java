package com.anapioficeandfire.gameofthrones.mvp.presenters;

import android.support.annotation.Nullable;

import com.anapioficeandfire.gameofthrones.mvp.models.CharacterModel;
import com.anapioficeandfire.gameofthrones.mvp.models.ICharacterModel;
import com.anapioficeandfire.gameofthrones.mvp.views.ICharacterView;

public class CharacterPresenter implements ICharacterPresenter {

    private static CharacterPresenter sCharacterPresenter = new CharacterPresenter();
    private ICharacterModel mCharacterModel;
    private ICharacterView mCharacterView;

    private CharacterPresenter() {
        mCharacterModel = CharacterModel.getInstance();
    }

    public static CharacterPresenter getInstance() {
        return sCharacterPresenter;
    }


    @Override
    public void takeView(ICharacterView characterView) {
        mCharacterView = characterView;
    }

    @Override
    public void dropView() {
        mCharacterView = null;
    }

    @Override
    public void initView() {
        if (getView() != null && getModel() != null) {
            ICharacterModel model = getModel();
            getView().showCharacter();

            if (model.getCharacterFather() != null) {
                getView().showFatherBtn(model.getCharacterFather().getName());
            }
            if (model.getCharacterMather() != null) {
                getView().showMatherBtn(model.getCharacterMather().getName());
            }
        }
    }

    @Nullable
    @Override
    public ICharacterView getView() {
        return mCharacterView;
    }

    @Override
    public ICharacterModel getModel() {
        return mCharacterModel;
    }

    @Override
    public void clickOnParent() {
        //TODO: 24.10.2016 сделать отображение родителей
        if (getView() != null && getModel() != null) {
            ICharacterModel model = getModel();
            if (model.getCharacterFather() != null) {
                getView().showFatherActivity(model.getCharacterFather().getId());
            }
        }
    }

    @Override
    public void clickOnMather() {
        if (getView() != null && getModel() != null) {
            ICharacterModel model = getModel();
            if (model.getCharacterMather() != null) {
                getView().showMatherActivity(model.getCharacterMather().getId());
            }
        }
    }
}
