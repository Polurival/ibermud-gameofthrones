package com.anapioficeandfire.gameofthrones.mvp.models;

import com.anapioficeandfire.gameofthrones.data.storage.models.House;

public interface IHouseModel {

    House getHouse(int id);

}
