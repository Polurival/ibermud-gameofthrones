package com.anapioficeandfire.gameofthrones.mvp.presenters;

import android.support.annotation.Nullable;

import com.anapioficeandfire.gameofthrones.mvp.models.ISplashModel;
import com.anapioficeandfire.gameofthrones.mvp.views.ISplashView;

public interface ISplashPresenter {

    void takeView(ISplashView splashView);
    void dropView();
    void initView();

    @Nullable
    ISplashView getView();
    ISplashModel getModel();

    boolean checkDatabase();

    void requestData();
}
