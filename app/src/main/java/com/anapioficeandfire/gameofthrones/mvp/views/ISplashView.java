package com.anapioficeandfire.gameofthrones.mvp.views;

import com.anapioficeandfire.gameofthrones.mvp.presenters.ISplashPresenter;

public interface ISplashView {

    void showMessage(String message);
    void showLoad();
    void hideLoad();

    ISplashPresenter getPresenter();
}
