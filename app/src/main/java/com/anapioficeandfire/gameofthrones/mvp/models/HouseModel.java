package com.anapioficeandfire.gameofthrones.mvp.models;

import com.anapioficeandfire.gameofthrones.data.managers.DataManager;
import com.anapioficeandfire.gameofthrones.data.storage.models.House;

public class HouseModel implements IHouseModel {

    private static HouseModel sHouseModel;
    private DataManager mDataManager;

    private HouseModel() {
        mDataManager = DataManager.getInstance();
    }

    public static HouseModel getInstance() {
        if (sHouseModel == null) {
            sHouseModel = new HouseModel();
        }
        return sHouseModel;
    }

    @Override
    public House getHouse(int id) {
        return mDataManager.getHouse(id);
    }
}
