package com.anapioficeandfire.gameofthrones.mvp.views;

import com.anapioficeandfire.gameofthrones.mvp.presenters.ICharacterPresenter;

public interface ICharacterView {
    void showCharacter();

    void showTitle(String title);
    void showMessage(String message);

    void showFatherBtn(String btnTitle);
    void showMatherBtn(String btnTitle);

    void showFatherActivity(int fatherId);
    void showMatherActivity(int matherId);

    ICharacterPresenter getPresenter();
}
