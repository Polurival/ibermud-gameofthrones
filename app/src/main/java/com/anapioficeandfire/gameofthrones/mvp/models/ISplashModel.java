package com.anapioficeandfire.gameofthrones.mvp.models;

import com.anapioficeandfire.gameofthrones.data.storage.models.Character;
import com.anapioficeandfire.gameofthrones.data.storage.models.House;

import java.util.List;

public interface ISplashModel {

    void loadData();

    List<House> getHouses();
    List<Character> getHouseMembers(House house);

    House getHouse(int id);
    Character getCharacter(int id);
}
