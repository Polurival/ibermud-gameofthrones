package com.anapioficeandfire.gameofthrones.mvp.views;

public interface IHouseView {

    void showCharacterActivity(int characterId);

}
