package com.anapioficeandfire.gameofthrones.mvp.models;

import android.support.annotation.Nullable;

import com.anapioficeandfire.gameofthrones.data.storage.models.Character;

public interface ICharacterModel {

    Character getCharacter(int characterId);

    @Nullable
    Character getCharacterFather();

    @Nullable
    Character getCharacterMather();

}
