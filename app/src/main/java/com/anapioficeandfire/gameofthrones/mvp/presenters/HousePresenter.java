package com.anapioficeandfire.gameofthrones.mvp.presenters;

import android.support.annotation.Nullable;

import com.anapioficeandfire.gameofthrones.mvp.models.HouseModel;
import com.anapioficeandfire.gameofthrones.mvp.models.IHouseModel;
import com.anapioficeandfire.gameofthrones.mvp.views.IHouseView;

public class HousePresenter implements IHousePresenter {

    private static HousePresenter sHousePresenter = new HousePresenter();
    private IHouseModel mHouseModel;
    private IHouseView mHouseView;

    private HousePresenter() {
        mHouseModel = HouseModel.getInstance();
    }

    public static HousePresenter getInstance() {
        return sHousePresenter;
    }


    @Override
    public void takeView(IHouseView houseView) {
        mHouseView = houseView;
    }

    @Override
    public void dropView() {
        mHouseView = null;
    }

    @Override
    public void initView() {

    }

    @Nullable
    @Override
    public IHouseView getView() {
        return mHouseView;
    }

    @Override
    public IHouseModel getModel() {
        return mHouseModel;
    }

    @Override
    public void clickOnHouse() {

    }

    @Override
    public void clickOnMember() {

    }
}
