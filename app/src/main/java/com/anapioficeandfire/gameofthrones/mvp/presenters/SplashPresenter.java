package com.anapioficeandfire.gameofthrones.mvp.presenters;

import android.support.annotation.Nullable;

import com.anapioficeandfire.gameofthrones.data.storage.models.House;
import com.anapioficeandfire.gameofthrones.mvp.models.SplashModel;
import com.anapioficeandfire.gameofthrones.mvp.models.ISplashModel;
import com.anapioficeandfire.gameofthrones.mvp.views.ISplashView;

import java.util.List;

/*
Как я понял у тебя SplashPresenter или SplashActivity 
никак не предупреждаются об окончании загрузки и сохранения ланных в бд.

Классический вариант - сделать где-то интерфейс, например:
interface OnDataPreparedListener() {
    void startMainActivity();
}
SplashPresenter будет имплементировать данный интерфейс и в методе startMainActivity() вызывать getView().startMainActivity();

В тот класс где у тебя сохранение в БД, надо из SplashPresenter пробросить OnDataPreparedListener, то есть this
и после сохранения последней порции данных вызвать OnDataPreparedListener.startMainActivity();

Возможно что-то я упустил, надо пробовать
*/
public class SplashPresenter implements ISplashPresenter { // implements OnDataPreparedListener;

    private static SplashPresenter sSplashPresenter;
    private ISplashModel mGotModel;
    private ISplashView mSplashView;

    private SplashPresenter() {
        mGotModel = SplashModel.getInstance();
    }

    public static SplashPresenter getInstance() {
        if (sSplashPresenter == null) {
            sSplashPresenter = new SplashPresenter();
        }
        return sSplashPresenter;
    }

    @Override
    public void takeView(ISplashView splashView) {
        mSplashView = splashView;
    }

    @Override
    public void dropView() {
        mSplashView = null;
    }

    @Override
    public void initView() {

    }

    @Nullable
    @Override
    public ISplashModel getModel() {
        return mGotModel;
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return mSplashView;
    }

    @Override
    public boolean checkDatabase() {
        List<House> houses = null;

        if (getModel() != null) {
            houses = getModel().getHouses();
        }
        return houses != null && houses.size() > 0;
    }

    @Override
    public void requestData() {
        if (getModel() != null) {
            getModel().loadData();
        }
    }

}
