package com.anapioficeandfire.gameofthrones.helpers;

import android.text.TextUtils;

import com.anapioficeandfire.gameofthrones.utils.ApplicationConfiguration;
import com.anapioficeandfire.gameofthrones.utils.ConstantsManager;

public class DataHelper {

    public static int getHouseIdFromUrl(String url) {
        return getIdFromUrl(url, ApplicationConfiguration.BASE_HOUSE_URL);
    }

    public static int getCharacterIdFromUrl(String url) {
        return getIdFromUrl(url, ApplicationConfiguration.BASE_CHARACTER_URL);
    }

    private static int getIdFromUrl(String url, String trimUrl) {
        int id = ConstantsManager.NULL_INDEX;
        try {
            if (!TextUtils.isEmpty(url)) {
                id = Integer.parseInt(url.replace(trimUrl, ""));
            }
        } catch (NumberFormatException e) {
        }
        return id;
    }

}
