package com.anapioficeandfire.gameofthrones.data.storage.models;

import android.text.TextUtils;

import com.anapioficeandfire.gameofthrones.R;
import com.anapioficeandfire.gameofthrones.network.requests.HouseModelRes;
import com.anapioficeandfire.gameofthrones.utils.ConstantsManager;

import java.util.ArrayList;
import java.util.List;

public class House {

    private int mIcon;

    private int mBackground;

    private int mId;

    private String mName;

    private String mWords;

    private String mTitles;

    private List<Character> mMembers = new ArrayList<>();

    private List<String> mMembersUrl = new ArrayList<>();

    public House(int id, HouseModelRes house) {
        if (house != null) {
            mId = id;
            mName = house.getName();
            mWords = house.getWords();
            mTitles = TextUtils.join("\n", house.getTitles());
            mMembersUrl = house.getSwornMembers();
        }
    }

    public House(int id, String name, String words, String titles) {
        mId = id;
        mName = name;
        mWords = words;
        mTitles = titles;
        switch (id) {
            case ConstantsManager.HOUSE_ID_LANNISTER:
                mIcon = R.drawable.img_icon_list_lanister;
                mBackground = R.drawable.img_header_lannister;
                break;
            case ConstantsManager.HOUSE_ID_STARK:
                mIcon = R.drawable.img_icon_list_stark;
                mBackground = R.drawable.img_header_stark;
                break;
            case ConstantsManager.HOUSE_ID_TARGARYEN:
                mIcon = R.drawable.img_icon_list_targarien;
                mBackground = R.drawable.img_header_targarien;
                break;
            default:
                mIcon = ConstantsManager.NULL_INDEX;
                mBackground = ConstantsManager.NULL_INDEX;
                break;
        }
    }

    public int getBackground() {
        return mBackground;
    }

    public int getIcon() {
        return mIcon;
    }

    public List<Character> getMembers() {
        return mMembers;
    }

    public void setMembers(List<Character> members) {
        mMembers = members;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getTitles() {
        return mTitles;
    }

    public void setTitles(String titles) {
        mTitles = titles;
    }

    public String getWords() {
        return mWords;
    }

    public void setWords(String words) {
        mWords = words;
    }

    public List<String> getMembersUrl() {
        return mMembersUrl;
    }

    public void setMembersUrl(List<String> membersUrl) {
        mMembersUrl = membersUrl;
    }

    @Override
    public String toString() {
        return TextUtils.isEmpty(this.mName) ?
                super.toString() : this.mName;
    }
}
