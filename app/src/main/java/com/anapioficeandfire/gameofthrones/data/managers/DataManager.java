package com.anapioficeandfire.gameofthrones.data.managers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.anapioficeandfire.gameofthrones.data.storage.models.Character;
import com.anapioficeandfire.gameofthrones.data.storage.models.House;
import com.anapioficeandfire.gameofthrones.database.CharacterCursorWrapper;
import com.anapioficeandfire.gameofthrones.database.GotBaseHelper;
import com.anapioficeandfire.gameofthrones.database.GotDbSchema;
import com.anapioficeandfire.gameofthrones.database.HouseCursorWrapper;
import com.anapioficeandfire.gameofthrones.helpers.DataHelper;
import com.anapioficeandfire.gameofthrones.network.RestService;
import com.anapioficeandfire.gameofthrones.network.ServiceGenerator;
import com.anapioficeandfire.gameofthrones.network.requests.CharacterModelRes;
import com.anapioficeandfire.gameofthrones.network.requests.HouseModelRes;
import com.anapioficeandfire.gameofthrones.utils.ConstantsManager;
import com.anapioficeandfire.gameofthrones.utils.GotApplication;
import com.anapioficeandfire.gameofthrones.utils.NetworkStatusChecker;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataManager {
    private static DataManager sDataManager = null;

    private PreferenceManager mPreferenceManager;
    private RestService mRestService;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    private DataManager() {
        mPreferenceManager = new PreferenceManager();
        mContext = GotApplication.getContext();
        mRestService = ServiceGenerator.createService(RestService.class);
        mDatabase = new GotBaseHelper(mContext).getWritableDatabase();
    }

    public static DataManager getInstance() {
        if (sDataManager == null) {
            sDataManager = new DataManager();
        }
        return sDataManager;
    }

    private static ContentValues getHouseContentValues(House house) {
        ContentValues values = new ContentValues();
        values.put(GotDbSchema.HouseTable.Columns.ID, house.getId());
        values.put(GotDbSchema.HouseTable.Columns.NAME, house.getName());
        values.put(GotDbSchema.HouseTable.Columns.WORDS, house.getWords());
        values.put(GotDbSchema.HouseTable.Columns.TITLES, house.getTitles());
        return values;
    }

    private static ContentValues getCharacterContentValues(Character character) {
        ContentValues values = new ContentValues();
        values.put(GotDbSchema.CharacterTable.Columns.ID, character.getId());
        values.put(GotDbSchema.CharacterTable.Columns.NAME, character.getName());
        values.put(GotDbSchema.CharacterTable.Columns.GENDER, character.getGender());
        values.put(GotDbSchema.CharacterTable.Columns.BORN, character.getBorn());
        values.put(GotDbSchema.CharacterTable.Columns.DIED, character.getDied());
        values.put(GotDbSchema.CharacterTable.Columns.TITLES, character.getTitles());
        values.put(GotDbSchema.CharacterTable.Columns.ALIASES, character.getAliases());
        values.put(GotDbSchema.CharacterTable.Columns.LAST_SEASON, character.getLastSeason());
        values.put(GotDbSchema.CharacterTable.Columns.FATHER_ID, character.getFatherId());
        values.put(GotDbSchema.CharacterTable.Columns.MOTHER_ID, character.getMotherId());
        values.put(GotDbSchema.CharacterTable.Columns.HOUSE_ID, character.getHouseId());

        return values;
    }

    public PreferenceManager getPreferenceManager() { return mPreferenceManager; }

    public Context getContext() { return mContext; }

    private void addHouse(House house) {
        if (house != null) {
            ContentValues hValues = getHouseContentValues(house);
            mDatabase.insert(GotDbSchema.HouseTable.NAME, null, hValues);
        }
    }

    private void addCharacter(Character character) {
        if (character != null) {
            ContentValues values = getCharacterContentValues(character);
            mDatabase.insert(GotDbSchema.CharacterTable.NAME, null, values);
        }
    }

    public House getHouse(int id) {
        HouseCursorWrapper cursor = queryHouses(
                GotDbSchema.HouseTable.Columns.ID + " = ?",
                new String[]{Integer.toString(id)}
        );
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            House house = cursor.getHouse();
            house.setMembers(getHouseMembers(house));
            return house;
        } finally {
            cursor.close();
        }
    }

    public Character getCharacter(int id) {
        CharacterCursorWrapper cursor = queryCharacters(
                GotDbSchema.CharacterTable.Columns.ID + " = ?",
                new String[]{Integer.toString(id)}
        );
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            Character character = cursor.getCharacter();
            House house = getHouse(character.getHouseId());
            character.setHouse(house);
            return character;
        } finally {
            cursor.close();
        }
    }

    /*
    Есть еще такой варинат перебора курсора, он короче и в одну строчку поэтому мне нравится:
    for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
        ...
    }
    cursor.close();
    */
    public List<House> getHouses() {
        List<House> houses = new ArrayList<>();
        HouseCursorWrapper cursor = queryHouses(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                House house = cursor.getHouse();
                house.setMembers(getHouseMembers(house));
                houses.add(house);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return houses;
    }

    public List<Character> getHouseMembers(House house) {
        List<Character> characters = new ArrayList<>();
        if (house != null) {
            CharacterCursorWrapper cursor = queryCharacters(
                    GotDbSchema.CharacterTable.Columns.HOUSE_ID + " = ?",
                    new String[]{Integer.toString(house.getId())}
            );
            try {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Character member = cursor.getCharacter();
                    member.setHouse(house);
                    characters.add(member);
                    cursor.moveToNext();
                }
            } finally {
                cursor.close();
            }
        }
        return characters;
    }

    private HouseCursorWrapper queryHouses(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                GotDbSchema.HouseTable.NAME,
                null, // Columns - null выбирает все столбцы
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                GotDbSchema.HouseTable.Columns.NAME // orderBy
        );

        return new HouseCursorWrapper(cursor);
    }

    private CharacterCursorWrapper queryCharacters(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                GotDbSchema.CharacterTable.NAME,
                null, // Columns - null выбирает все столбцы
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                GotDbSchema.CharacterTable.Columns.NAME // orderBy
        );

        return new CharacterCursorWrapper(cursor);
    }

    private void clearDatabase() {
        mDatabase.execSQL(String.format("DELETE FROM `%1$s`", GotDbSchema.CharacterTable.NAME));
        mDatabase.execSQL(String.format("DELETE FROM `%1$s`", GotDbSchema.HouseTable.NAME));
    }

    private Call<HouseModelRes> getHouseById(int houseId) {
        return mRestService.getHouseById(houseId);
    }

    private Call<CharacterModelRes> getCharacterById(int characterId) {
        return mRestService.getCharacterById(characterId);
    }

    private void requestHousesFromNetwork() {
        try {
            if (NetworkStatusChecker.isNetworkAvailable(mContext)) {
                for (final int houseId : ConstantsManager.MAIN_HOUSES) {
                    Call<HouseModelRes> call = getHouseById(houseId);
                    call.enqueue(new Callback<HouseModelRes>() {
                        @Override
                        public void onResponse(Call<HouseModelRes> call, Response<HouseModelRes> response) {
                            if (response != null && response.code() == 200) {
                                House house = new House(houseId, response.body());
                                addHouse(house);
                                requestHouseMembersFromNetwork(house);
                            }
                        }

                        @Override
                        public void onFailure(Call<HouseModelRes> call, Throwable t) {
                            //TODO: 24.10.2016 обработать ошибки
                        }
                    });
                }
            } else {
                throw new Exception("Нет подключения к сети");
            }
        } catch (Exception e) {
        }
    }

    private void requestCharacterFromNetwork(final int characterId, final int houseId) {
        try {
            if (NetworkStatusChecker.isNetworkAvailable(mContext)) {
                if (characterId != ConstantsManager.NULL_INDEX) {
                    Call<CharacterModelRes> call = getCharacterById(characterId);
                    call.enqueue(new Callback<CharacterModelRes>() {
                        @Override
                        public void onResponse(Call<CharacterModelRes> call,
                                Response<CharacterModelRes> response) {
                            if (response != null && response.code() == 200) {
                                Character member = new Character(characterId,
                                        response.body(), houseId);
                                addCharacter(member);

                                //TODO: 24.10.2016 задавать реальные идентификаторы домов
                                if (member.getFatherId() != ConstantsManager.NULL_INDEX) {
                                    requestCharacterFromNetwork(member.getFatherId(), houseId);
                                }

                                if (member.getMotherId() != ConstantsManager.NULL_INDEX) {
                                    requestCharacterFromNetwork(member.getMotherId(), houseId);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<CharacterModelRes> call, Throwable t) {
                            //TODO: 24.10.2016 обработать ошибки
                        }
                    });
                }
            } else {
                throw new Exception("Нет подключения к сети");
            }
        } catch (Exception e) {
        }
    }

    private void requestHouseMembersFromNetwork(final House house) {
        if (house != null) {
            List<String> membersUrl = house.getMembersUrl();
            if (membersUrl != null) {
                for (String memberUrl : membersUrl) {
                    int memberId = DataHelper.getCharacterIdFromUrl(memberUrl);
                    requestCharacterFromNetwork(memberId, house.getId());
                }
            }
        }
    }

    public void loadData() {
        clearDatabase();

        requestHousesFromNetwork();
    }

}
