package com.anapioficeandfire.gameofthrones.data.storage.models;

import android.text.TextUtils;

import com.anapioficeandfire.gameofthrones.R;
import com.anapioficeandfire.gameofthrones.data.managers.DataManager;
import com.anapioficeandfire.gameofthrones.helpers.DataHelper;
import com.anapioficeandfire.gameofthrones.network.requests.CharacterModelRes;

import java.util.List;

public class Character {

    private int mId;

    private String mName;

    private String mGender;

    private String mBorn;

    private String mDied;

    private String mTitles;

    private String mAliases;

    private String mLastSeason;

    private int mFatherId;

    private int mMotherId;

    private int mHouseId;

    private House mHouse;

    public Character(int id, String name, String gender, String born, String died, String titles,
            String aliases, String lastSeason, int fatherId, int matherId, int houseId) {
        this.mId = id;
        this.mName = name;
        this.mGender = gender;
        this.mBorn = born;
        this.mDied = died;
        this.mTitles = titles;
        this.mAliases = aliases;
        this.mFatherId = fatherId;
        this.mMotherId = matherId;
        this.mHouseId = houseId;
        this.mLastSeason = lastSeason;
    }

    public Character(int id, CharacterModelRes character, int houseId) {
        if (character != null) {
            this.mId = id;
            this.mName = character.getName();
            this.mGender = character.getGender();
            this.mBorn = character.getBorn();
            this.mDied = character.getDied();
            this.mTitles = TextUtils.join("\n", character.getTitles());
            this.mAliases = TextUtils.join("\n", character.getAliases());
            this.mFatherId = DataHelper.getCharacterIdFromUrl(
                    character.getFather());
            this.mMotherId = DataHelper.getCharacterIdFromUrl(
                    character.getMother());

            List<Object> seasons = character.getTvSeries();

            if (seasons != null && seasons.size() > 0) {
                this.mLastSeason = seasons.get(seasons.size() - 1).toString();
            }

            this.mHouseId = houseId;
        }
    }

    public String getAliases() {
        return mAliases;
    }

    public void setAliases(String aliases) {
        mAliases = aliases;
    }

    public String getBorn() {
        return mBorn;
    }

    public void setBorn(String born) {
        mBorn = born;
    }

    public String getDied() {
        return mDied;
    }

    public void setDied(String died) {
        mDied = died;
    }

    public int getFatherId() {
        return mFatherId;
    }

    public void setFatherId(int fatherId) {
        mFatherId = fatherId;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public House getHouse() {
        return mHouse;
    }

    public void setHouse(House house) {
        mHouse = house;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getLastSeason() {
        return mLastSeason;
    }

    public void setLastSeason(String lastSeason) {
        mLastSeason = lastSeason;
    }

    public int getMotherId() {
        return mMotherId;
    }

    public void setMotherId(int motherId) {
        mMotherId = motherId;
    }

    public String getName() {
        return TextUtils.isEmpty(this.mName) ?
                DataManager.getInstance().getContext().getResources().getString(
                        R.string.character_no_name) : this.mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getTitles() {
        return mTitles;
    }

    public void setTitles(String titles) {
        mTitles = titles;
    }

    public int getHouseId() {
        return mHouseId;
    }

    public void setHouseId(int houseId) {
        mHouseId = houseId;
    }

    public String getExtra() {
        return TextUtils.isEmpty(this.mAliases) ? this.mTitles : this.mAliases;
    }

    @Override
    public String toString() {
        return TextUtils.isEmpty(this.mName) ?
                super.toString() : this.mName;
    }

}
