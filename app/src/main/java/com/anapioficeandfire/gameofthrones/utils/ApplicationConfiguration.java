package com.anapioficeandfire.gameofthrones.utils;

public interface ApplicationConfiguration {
    String BASE_URL = "http://anapioficeandfire.com/api/";
    String BASE_HOUSE_URL = BASE_URL + "house/";
    String BASE_CHARACTER_URL = BASE_URL + "characters/";

    int MAX_CONNECT_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;

    int START_DELAY = 3500;
}
