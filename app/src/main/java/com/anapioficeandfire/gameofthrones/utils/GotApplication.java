package com.anapioficeandfire.gameofthrones.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class GotApplication extends Application {

    private static SharedPreferences sSharedPreferences;
    private static Context sContext;

    public static SharedPreferences getSharedPreferences() { return sSharedPreferences; }

    public static Context getContext() { return sContext; }

    @Override
    public void onCreate() {
        super.onCreate();
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sContext = this;
    }
}