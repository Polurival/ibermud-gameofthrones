package com.anapioficeandfire.gameofthrones.utils;

public interface ConstantsManager {

    /*Fragment arguments*/
    String ARG_HOUSE_ID = "house_id";
    String ARG_CHARACTER_ID = "character_id";

    /*Intent extras*/
    String EXTRA_HOUSE_ID = "com.anapioficeandfire.gameofthrones.house_id";
    String EXTRA_CHARACTER_ID = "com.anapioficeandfire.gameofthrones.character_id";

    int NULL_INDEX = -1;

    String TAG_PREFIX = "DEV ";

    String HOUSE_TAB_STARK = "STARK";

    String HOUSE_TAB_LANNISTER = "LANNISTER";

    String HOUSE_TAB_TARGARYEN = "TARGARYEN";

    int HOUSE_ID_STARK = 362;

    int HOUSE_ID_LANNISTER = 229;

    int HOUSE_ID_TARGARYEN = 378;

    int[] MAIN_HOUSES = { HOUSE_ID_STARK, HOUSE_ID_LANNISTER, HOUSE_ID_TARGARYEN };

    //int[] MAIN_HOUSES = { HOUSE_ID_STARK };
    /*
    String HOUSE_NAME_STARK = "House Stark of Winterfell";

    String HOUSE_NAME_LANNISTER = "House Lannister of Casterly Rock";

    String HOUSE_NAME_TARGARYEN = "House Targaryen of King's Landing";

    String[] MAIN_HOUSES = { HOUSE_NAME_STARK, HOUSE_NAME_LANNISTER, HOUSE_NAME_TARGARYEN };
    */
}
