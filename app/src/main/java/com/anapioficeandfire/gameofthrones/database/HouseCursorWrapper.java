package com.anapioficeandfire.gameofthrones.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.anapioficeandfire.gameofthrones.data.storage.models.House;
import com.anapioficeandfire.gameofthrones.database.GotDbSchema.HouseTable;

public class HouseCursorWrapper extends CursorWrapper {
    public HouseCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public House getHouse() {
        int id = getInt(getColumnIndex(HouseTable.Columns.ID));
        String name = getString(getColumnIndex(HouseTable.Columns.NAME));
        String words = getString(getColumnIndex(HouseTable.Columns.WORDS));
        String titles = getString(getColumnIndex(HouseTable.Columns.TITLES));

        return new House(id, name, words, titles);
    }
}
