package com.anapioficeandfire.gameofthrones.database;

public class GotDbSchema {

    public static final class HouseTable {
        public static final String NAME = "house";

        public static final class Columns {
            public static final String ID = "id";
            public static final String NAME = "name";
            public static final String WORDS = "words";
            public static final String TITLES = "titles";
        }
    }

    public static final class CharacterTable {
        public static final String NAME = "character";

        public static final class Columns {
            public static final String ID = "id";
            public static final String NAME = "name";
            public static final String GENDER = "gender";
            public static final String BORN = "born";
            public static final String DIED = "died";
            public static final String TITLES = "titles";
            public static final String ALIASES = "aliases";
            public static final String LAST_SEASON = "last_season";
            public static final String URL = "url";
            public static final String FATHER_ID = "father_id";
            public static final String MOTHER_ID = "mother_id";
            public static final String HOUSE_ID = "house_id";
        }
    }

}
