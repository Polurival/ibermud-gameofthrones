package com.anapioficeandfire.gameofthrones.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.anapioficeandfire.gameofthrones.database.GotDbSchema.CharacterTable;
import com.anapioficeandfire.gameofthrones.database.GotDbSchema.HouseTable;

public class GotBaseHelper extends SQLiteOpenHelper {

    /**
     * Текущая версия базы данных
     */
    private static final int VERSION = 1;

    /**
     * Имя файла базы данных
     */
    private static final String DATABASE_NAME = "gameOfThroneBase.db";

    public GotBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*
        switch (oldVersion) {
            case 1:
            case 2:
        }
        */
        db.setVersion(VERSION);
    }

    private void createTables(SQLiteDatabase db) {
        db.execSQL(String.format(
                "CREATE TABLE `%1$s` (" +
                        "`%2$s` INTEGER, " +
                        "`%3$s` TEXT, " +
                        "`%4$s` TEXT, " +
                        "`%5$s` TEXT)",
                HouseTable.NAME,
                HouseTable.Columns.ID,
                HouseTable.Columns.NAME,
                HouseTable.Columns.WORDS,
                HouseTable.Columns.TITLES));

        db.execSQL(String.format(
                "CREATE TABLE `%1$s` (" +
                        "`%2$s` INTEGER, " +
                        "`%3$s` TEXT, " +
                        "`%4$s` TEXT, " +
                        "`%5$s` TEXT, " +
                        "`%6$s` TEXT, " +
                        "`%7$s` TEXT, " +
                        "`%8$s` TEXT, " +
                        "`%9$s` TEXT, " +
                        "`%10$s` INTEGER, " +
                        "`%11$s` INTEGER, " +
                        "`%12$s` INTEGER)",
                CharacterTable.NAME,
                CharacterTable.Columns.ID,
                CharacterTable.Columns.NAME,
                CharacterTable.Columns.GENDER,
                CharacterTable.Columns.BORN,
                CharacterTable.Columns.DIED,
                CharacterTable.Columns.TITLES,
                CharacterTable.Columns.ALIASES,
                CharacterTable.Columns.LAST_SEASON,
                CharacterTable.Columns.FATHER_ID,
                CharacterTable.Columns.MOTHER_ID,
                CharacterTable.Columns.HOUSE_ID));
    }


}

