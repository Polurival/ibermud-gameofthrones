package com.anapioficeandfire.gameofthrones.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.anapioficeandfire.gameofthrones.data.storage.models.Character;
import com.anapioficeandfire.gameofthrones.database.GotDbSchema.CharacterTable;

public class CharacterCursorWrapper extends CursorWrapper {
    public CharacterCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Character getCharacter() {
        int id = getInt(getColumnIndex(CharacterTable.Columns.ID));
        String name = getString(getColumnIndex(CharacterTable.Columns.NAME));
        String gender = getString(getColumnIndex(CharacterTable.Columns.GENDER));
        String born = getString(getColumnIndex(CharacterTable.Columns.BORN));
        String died = getString(getColumnIndex(CharacterTable.Columns.DIED));
        String titles = getString(getColumnIndex(CharacterTable.Columns.TITLES));
        String aliases = getString(getColumnIndex(CharacterTable.Columns.ALIASES));
        String lastSeason = getString(getColumnIndex(CharacterTable.Columns.LAST_SEASON));
        int fatherId = getInt(getColumnIndex(CharacterTable.Columns.FATHER_ID));
        int motherId = getInt(getColumnIndex(CharacterTable.Columns.MOTHER_ID));
        int houseId = getInt(getColumnIndex(CharacterTable.Columns.HOUSE_ID));

        return new Character(id, name, gender, born, died, titles, aliases,
                lastSeason, fatherId, motherId, houseId);
    }


}
