package com.anapioficeandfire.gameofthrones.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.anapioficeandfire.gameofthrones.R;
import com.anapioficeandfire.gameofthrones.mvp.presenters.ISplashPresenter;
import com.anapioficeandfire.gameofthrones.mvp.presenters.SplashPresenter;
import com.anapioficeandfire.gameofthrones.mvp.views.ISplashView;

public class SplashActivity extends AppCompatActivity implements ISplashView{
    private static final String TAG = "SplashActivity";

    private SplashPresenter mSplashPresenter = SplashPresenter.getInstance();
    private ProgressDialog mProgress;
    private FrameLayout mFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        mFrameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.splash_view_loading));

        mSplashPresenter.takeView(this);
        mSplashPresenter.initView();

        int delay = 2000;
        showLoad();

        boolean isContainData = mSplashPresenter.checkDatabase();

        if (!isContainData) {
            delay = 10000;
            mSplashPresenter.requestData();
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showCharacterListActivity();
            }
        }, delay);

        hideLoad();
    }

    @Override
    protected void onDestroy() {
        mSplashPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mFrameLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoad() {
        mProgress.show();
    }

    @Override
    public void hideLoad() {
        mProgress.hide();
    }

    @Override
    public ISplashPresenter getPresenter() {
        return mSplashPresenter;
    }

    private void showCharacterListActivity(){
        startActivity(new Intent(SplashActivity.this, HouseListActivity.class));
        finish();
    }

}
