package com.anapioficeandfire.gameofthrones.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.anapioficeandfire.gameofthrones.R;
import com.anapioficeandfire.gameofthrones.data.storage.models.Character;
import com.anapioficeandfire.gameofthrones.data.storage.models.House;
import com.anapioficeandfire.gameofthrones.mvp.presenters.CharacterPresenter;
import com.anapioficeandfire.gameofthrones.ui.fragments.CharacterFragment;
import com.anapioficeandfire.gameofthrones.utils.ConstantsManager;

import java.util.ArrayList;
import java.util.List;

public class CharacterActivity extends AppCompatActivity {
    private static final String TAG = "CharacterActivity";

    private CharacterPresenter mCharacterPresenter = CharacterPresenter.getInstance();
    private List<Character> mCharacters;

    public static Intent newIntent(Context packageContext, int memberId) {
        Intent intent = new Intent(packageContext, CharacterActivity.class);
        intent.putExtra(ConstantsManager.EXTRA_CHARACTER_ID, memberId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);

        final int characterId = getIntent().getIntExtra(ConstantsManager.EXTRA_CHARACTER_ID,
                ConstantsManager.NULL_INDEX);

        mCharacters = new ArrayList<>();
        Character character = mCharacterPresenter.getModel().getCharacter(characterId);

        if (character != null) {
            House house = character.getHouse();
            if (house != null) {
                mCharacters = house.getMembers();
            }
        }

        ViewPager viewPager = (ViewPager) findViewById(R.id.character_view_pager);
        viewPager.setOffscreenPageLimit(1);

        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Character character = mCharacters.get(position);
                return CharacterFragment.newInstance(character.getId());
            }

            @Override
            public int getCount() {
                return mCharacters.size();
            }
        });


        for (int i = 0; i < mCharacters.size(); i++) {
            if (mCharacters.get(i).getId() == characterId) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }
}
