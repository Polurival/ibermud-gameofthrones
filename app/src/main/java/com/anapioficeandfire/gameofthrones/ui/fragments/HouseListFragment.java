package com.anapioficeandfire.gameofthrones.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anapioficeandfire.gameofthrones.R;
import com.anapioficeandfire.gameofthrones.data.storage.models.Character;
import com.anapioficeandfire.gameofthrones.data.storage.models.House;
import com.anapioficeandfire.gameofthrones.mvp.presenters.HousePresenter;
import com.anapioficeandfire.gameofthrones.mvp.views.IHouseView;
import com.anapioficeandfire.gameofthrones.ui.activities.CharacterActivity;
import com.anapioficeandfire.gameofthrones.ui.decorations.DividerItemDecoration;
import com.anapioficeandfire.gameofthrones.utils.ConstantsManager;

import java.util.ArrayList;
import java.util.List;

public class HouseListFragment extends Fragment implements IHouseView {

    private HousePresenter mHousePresenter = HousePresenter.getInstance();

    private RecyclerView mRecyclerView;
    private CharacterAdapter mCharacterAdapter;

    private House mHouse;

    public static HouseListFragment newInstance(int houseId) {
        Bundle args = new Bundle();
        args.putInt(ConstantsManager.ARG_HOUSE_ID, houseId);
        HouseListFragment fragment = new HouseListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        int houseId = getArguments().getInt(ConstantsManager.ARG_HOUSE_ID);
        mHouse = mHousePresenter.getModel().getHouse(houseId);

        View view = inflater.inflate(R.layout.fragment_character_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.character_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(
                new DividerItemDecoration(getContext().getResources()));

        mHousePresenter.takeView(this);
        mHousePresenter.initView();

        updateRecycleView();
        setupActionBar();
        return view;
    }

    @Override
    public void onDestroyView() {
        mHousePresenter.dropView();
        super.onDestroyView();
    }

    @Override
    public void showCharacterActivity(int characterId) {
        Intent intent = CharacterActivity.newIntent(getActivity(), characterId);
        startActivity(intent);
    }

    public void updateRecycleView() {
        List<Character> characters = new ArrayList<>();
        if (mHouse != null) {
            characters = mHouse.getMembers();
        }

        if (mCharacterAdapter == null) {
            mCharacterAdapter = new CharacterAdapter(characters);
            mRecyclerView.setAdapter(mCharacterAdapter);
        } else {
            mCharacterAdapter.setCharacters(characters);
        }
    }

    private void setupActionBar() {
        AppCompatActivity activity = (AppCompatActivity) getContext();
        ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public class CharacterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Character mCharacter;

        private ImageView mIconImageView;
        private TextView mNameTextView;
        private TextView mAlisesTextView;

        CharacterHolder(View itemView) {
            super(itemView);

            mIconImageView = (ImageView) itemView.findViewById(R.id.character_icon_image_view);
            mNameTextView = (TextView) itemView.findViewById(R.id.character_name_text_view);
            mAlisesTextView = (TextView) itemView.findViewById(R.id.character_aliases_text_view);

            itemView.setOnClickListener(this);
        }

        void bindCharacter(Character character) {
            mCharacter = character;

            House house = mCharacter.getHouse();
            if (house != null) {
                mIconImageView.setImageResource(house.getIcon());
            }

            mNameTextView.setText(mCharacter.getName());
            mAlisesTextView.setText(mCharacter.getExtra());
        }

        @Override
        public void onClick(View v) {
            showCharacterActivity(mCharacter.getId());
        }
    }

    private class CharacterAdapter extends RecyclerView.Adapter<CharacterHolder>{

        private List<Character> mCharacters;

        CharacterAdapter(List<Character> characters) {
            mCharacters = characters;
        }

        @Override
        public int getItemCount() {
            return mCharacters.size();
        }

        @Override
        public CharacterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater =
                    LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.list_item_character, parent, false);
            return new CharacterHolder(view);
        }

        @Override
        public void onBindViewHolder(CharacterHolder holder, int position) {
            Character character = mCharacters.get(position);
            holder.bindCharacter(character);
        }

        void setCharacters(List<Character> characters) {
            mCharacters = characters;
        }
    }
}
