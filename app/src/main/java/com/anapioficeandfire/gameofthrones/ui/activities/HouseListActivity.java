package com.anapioficeandfire.gameofthrones.ui.activities;

import android.os.Bundle;

import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseIntArray;
import android.view.MenuItem;

import com.anapioficeandfire.gameofthrones.R;
import com.anapioficeandfire.gameofthrones.ui.adapters.CharacterListAdapter;
import com.anapioficeandfire.gameofthrones.utils.ConstantsManager;

public class HouseListActivity extends AppCompatActivity {
    private static final String TAG = "HouseListActivity";

    private DrawerLayout mNavigationDrawer;
    private NavigationView mNavigationView;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private CharacterListAdapter mCharacterListAdapter;

    private SparseIntArray mDictionaryHouseIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);
        fillDictionaryHouseIds();

        mNavigationDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer);

        setupToolBar();
        setupViewPager();
        setupNavigationDrawer();
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawer.isDrawerOpen(GravityCompat.START)) {
            mNavigationDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mNavigationDrawer.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    private void selectTabHousePage(int houseId) {
        if (mCharacterListAdapter != null) {
            int pageIndex = mCharacterListAdapter.getIndex(houseId);
            if (pageIndex != ConstantsManager.NULL_INDEX) {
                mTabLayout.setScrollPosition(pageIndex, 0f, true);
                mViewPager.setCurrentItem(pageIndex);
            }
        }
    }

    private void selectMenuHouseItem(int itemId) {
        if (mNavigationView != null && mNavigationView.getMenu() != null) {
            MenuItem item = mNavigationView.getMenu().findItem(itemId);
            if (item != null) {
                item.setChecked(true);
            }
        }
    }

    private void setupNavigationDrawer() {
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_item_stark:
                        selectTabHousePage(ConstantsManager.HOUSE_ID_STARK);
                        break;
                    case R.id.menu_item_lannister:
                        selectTabHousePage(ConstantsManager.HOUSE_ID_LANNISTER);
                        break;
                    case R.id.menu_item_targaryen:
                        selectTabHousePage(ConstantsManager.HOUSE_ID_TARGARYEN);
                        break;
                }
                item.setChecked(true);
                mNavigationDrawer.closeDrawer(GravityCompat.START);
                return false;
            }
        });

        if (mViewPager != null && mCharacterListAdapter != null) {
            int currentItem = mViewPager.getCurrentItem();

            if (currentItem < mCharacterListAdapter.getCount()) {
                int houseId = mCharacterListAdapter.getHouseId(currentItem);
                selectMenuHouseItem(mDictionaryHouseIds.get(houseId));
            }
        }
    }

    private void setupToolBar() {
        if (getSupportActionBar() != null) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setupViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setOffscreenPageLimit(3);

        mCharacterListAdapter = new CharacterListAdapter(getSupportFragmentManager());
        mCharacterListAdapter.addCharacterListFragment(ConstantsManager.HOUSE_ID_STARK,
                ConstantsManager.HOUSE_TAB_STARK);
        mCharacterListAdapter.addCharacterListFragment(ConstantsManager.HOUSE_ID_LANNISTER,
                ConstantsManager.HOUSE_TAB_LANNISTER);
        mCharacterListAdapter.addCharacterListFragment(ConstantsManager.HOUSE_ID_TARGARYEN,
                ConstantsManager.HOUSE_TAB_TARGARYEN);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                    int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                int houseId = mCharacterListAdapter.getHouseId(position);
                int menuItemId = mDictionaryHouseIds.get(houseId);
                selectMenuHouseItem(menuItemId);
            }

            @Override
            public void onPageScrollStateChanged(int state) { }
        });
        mViewPager.setAdapter(mCharacterListAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void fillDictionaryHouseIds() {
        mDictionaryHouseIds = new SparseIntArray();
        mDictionaryHouseIds.append(ConstantsManager.HOUSE_ID_STARK, R.id.menu_item_stark);
        mDictionaryHouseIds.append(ConstantsManager.HOUSE_ID_LANNISTER, R.id.menu_item_lannister);
        mDictionaryHouseIds.append(ConstantsManager.HOUSE_ID_TARGARYEN, R.id.menu_item_targaryen);
    }
}
