package com.anapioficeandfire.gameofthrones.ui.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.anapioficeandfire.gameofthrones.ui.fragments.HouseListFragment;
import com.anapioficeandfire.gameofthrones.utils.ConstantsManager;

import java.util.ArrayList;
import java.util.List;

public class CharacterListAdapter extends FragmentPagerAdapter {
    private List<HouseListFragment> mFragmentList = new ArrayList<>();
    private List<String> mFragmentTitleList = new ArrayList<>();
    private List<Integer> mFragmentIdList = new ArrayList<>();

    public CharacterListAdapter(FragmentManager manager) {
        super(manager);
    }

    public int getIndex(int houseId) {
        return mFragmentIdList.contains(houseId) ?
                mFragmentIdList.indexOf(houseId) : ConstantsManager.NULL_INDEX;
    }

    public int getHouseId(int position) {
        return mFragmentIdList.get(position);
    }

    @Override
    public HouseListFragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addCharacterListFragment(int houseId, String houseTitle) {
        mFragmentList.add(HouseListFragment.newInstance(houseId));
        mFragmentTitleList.add(houseTitle);
        mFragmentIdList.add(houseId);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
