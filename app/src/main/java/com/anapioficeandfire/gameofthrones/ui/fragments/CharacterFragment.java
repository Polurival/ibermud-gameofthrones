package com.anapioficeandfire.gameofthrones.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anapioficeandfire.gameofthrones.R;
import com.anapioficeandfire.gameofthrones.data.managers.DataManager;
import com.anapioficeandfire.gameofthrones.data.storage.models.Character;
import com.anapioficeandfire.gameofthrones.mvp.models.SplashModel;
import com.anapioficeandfire.gameofthrones.mvp.presenters.CharacterPresenter;
import com.anapioficeandfire.gameofthrones.mvp.presenters.ICharacterPresenter;
import com.anapioficeandfire.gameofthrones.mvp.views.ICharacterView;
import com.anapioficeandfire.gameofthrones.network.requests.CharacterModelRes;
import com.anapioficeandfire.gameofthrones.ui.activities.CharacterActivity;
import com.anapioficeandfire.gameofthrones.utils.ConstantsManager;
import com.anapioficeandfire.gameofthrones.utils.NetworkStatusChecker;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterFragment extends Fragment
        implements ICharacterView, View.OnClickListener {

    Character mCharacter;
    private CharacterPresenter mCharacterPresenter = CharacterPresenter.getInstance();
    private TextView mWordsTextView;
    private TextView mBornTextView;
    private TextView mDiedTextView;
    private TextView mLastAppearance;
    private TextView mTitlesTextView;
    private TextView mAliasesTextView;
    private AppCompatButton mFatherAppButton;
    private AppCompatButton mMotherAppButton;
    private ImageView mBackgroundImageView;
    private CoordinatorLayout mCoordinatorLayout;
    private CollapsingToolbarLayout mCollapsingToolbar;
    private AppBarLayout mAppBarLayout;
    private AppBarLayout.LayoutParams mAppBarParams = null;
    private Toolbar mToolbar;
    private View mView;

    public static CharacterFragment newInstance(int memberId) {
        Bundle args = new Bundle();
        args.putInt(ConstantsManager.ARG_CHARACTER_ID, memberId);
        CharacterFragment fragment = new CharacterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int memberId = getArguments().getInt(ConstantsManager.ARG_CHARACTER_ID,
                ConstantsManager.NULL_INDEX);

        mCharacter = getPresenter().getModel().getCharacter(memberId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_character, container, false);
        mCoordinatorLayout = (CoordinatorLayout) mView.findViewById(R.id.main_coord_container);
        mWordsTextView = (TextView) mView.findViewById(R.id.character_words_text_view);
        mBornTextView = (TextView) mView.findViewById(R.id.character_born_text_view);
        mDiedTextView = (TextView) mView.findViewById(R.id.character_died_text_view);
        mLastAppearance = (TextView) mView.findViewById(R.id.character_last_appearance_text_view);
        mTitlesTextView = (TextView) mView.findViewById(R.id.character_titles_text_view);
        mAliasesTextView = (TextView) mView.findViewById(R.id.character_aliases_text_view);
        mFatherAppButton = (AppCompatButton) mView.findViewById(R.id.character_father_button);
        mMotherAppButton = (AppCompatButton) mView.findViewById(R.id.character_mother_button);
        mBackgroundImageView = (ImageView) mView.findViewById(R.id.backround_photo_img);
        mCollapsingToolbar = (CollapsingToolbarLayout) mView.findViewById(R.id.collapsing_toolbar);
        mAppBarLayout = (AppBarLayout) mView.findViewById(R.id.appbar_layout);
        mToolbar = (Toolbar) mView.findViewById(R.id.toolbar);

        mCharacterPresenter.takeView(this);
        mCharacterPresenter.initView();

        mFatherAppButton.setOnClickListener(this);
        mMotherAppButton.setOnClickListener(this);

        setupToolbar();

        return mView;
    }

    //TODO: 24.10.2016 выводить снэкбар с сезоном в котором погиб персонаж
    /*
    private void showMemberStatus(Character character) {
        if (!TextUtils.isEmpty(character.getDied())) {
            String lastSeason = "";
            if (!TextUtils.isEmpty(character.getLastSeason())) {
                lastSeason = "\nПоследнее появление в " + character.getLastSeason();
            }
            showToast("Персонаж умер" + lastSeason);
        }
    }
    */

    @Override
    public void onDestroyView() {
        mCharacterPresenter.dropView();
        super.onDestroyView();
    }

    private void setupToolbar() {
        mAppBarParams = (AppBarLayout.LayoutParams) mCollapsingToolbar.getLayoutParams();
        mAppBarParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL |
                AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED);
        mCollapsingToolbar.setLayoutParams(mAppBarParams);
        mCollapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.color_white));
    }

    @Override
    public void showCharacter() {
        if (mCharacter != null) {
            int idImage = R.drawable.img_background_start;
            if (mCharacter.getHouse() != null) {
                mWordsTextView.setText(mCharacter.getHouse().getWords());
                if (mCharacter.getHouse().getBackground() != ConstantsManager.NULL_INDEX) {
                    idImage = mCharacter.getHouse().getBackground();
                }
            }
            Picasso.with(getContext())
                    .load(mCharacter.getHouse().getBackground())
                    //.fit()
                    .into(mBackgroundImageView);

            mBackgroundImageView.setImageResource(idImage);
            mBornTextView.setText(mCharacter.getBorn());
            mDiedTextView.setText(mCharacter.getDied());
            mLastAppearance.setText(mCharacter.getLastSeason());
            mTitlesTextView.setText(mCharacter.getTitles());
            mAliasesTextView.setText(mCharacter.getAliases());
            mFatherAppButton.setVisibility(View.GONE);
            mMotherAppButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void showTitle(String title) {
        //TODO: 25.10.2016 не отображается тайтл. починить
        if (!TextUtils.isEmpty(title)) {
            AppCompatActivity activity = (AppCompatActivity) getContext();

            activity.setSupportActionBar(mToolbar);
            ActionBar actionBar = activity.getSupportActionBar();

            if (actionBar != null) {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle(title);
            }
        }
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showFatherBtn(String fatherName) {
        if (!TextUtils.isEmpty(fatherName)) {
            mFatherAppButton.setText(fatherName);
            mFatherAppButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showMatherBtn(String matherName) {
        if (!TextUtils.isEmpty(matherName)) {
            mMotherAppButton.setText(matherName);
            mMotherAppButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showFatherActivity(int fatherId) {
        showCharacterActivity(fatherId);
    }

    @Override
    public void showMatherActivity(int matherId) {
        showCharacterActivity(matherId);
    }

    private void showCharacterActivity(int characterId){
        if (characterId != ConstantsManager.NULL_INDEX) {
            Intent intent = CharacterActivity.newIntent(getContext(),
                    characterId);
            startActivity(intent);
        }
    }

    @Override
    public ICharacterPresenter getPresenter() {
        return mCharacterPresenter;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.character_father_button:
                mCharacterPresenter.clickOnParent();
                break;
            case R.id.character_mother_button:
                mCharacterPresenter.clickOnMather();
                break;
        }
    }

}
